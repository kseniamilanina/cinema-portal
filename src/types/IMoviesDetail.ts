import React from "react";
import { JsxElement } from "typescript";

export interface IMoviesDetail {
        id: number;
        url: string;
        name: string;
        genres: string[];
        premiered: string;
        image: {
        medium: string;
        original: string;
        };
        network: {country: {
            name: string;
            code: string;
            timezone: string;
        }}; 
        summary: string;
        rating:{
            average: number;
        }
    }
    // React.TextareaHTMLAttributes<HTMLParagraphElement>