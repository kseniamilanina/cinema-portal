import axios, {AxiosResponse, AxiosRequestConfig} from "axios";

const getMoviesDetail = (id:string):Promise<AxiosResponse> => axios.get(`https://api.tvmaze.com/shows/${id}`);

export default getMoviesDetail;