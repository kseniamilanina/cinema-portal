import axios, {AxiosResponse, AxiosRequestConfig} from "axios";

const getMovies = (type = 'girls'):Promise<AxiosResponse> => axios.get(`https://api.tvmaze.com/search/shows?q=${type}`);

export default getMovies; 