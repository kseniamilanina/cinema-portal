import axios, {AxiosResponse, AxiosRequestConfig} from "axios";

const getMoviesCategory = (genre = 'animals'):Promise<AxiosResponse> => axios.get(`https://api.tvmaze.com/shows?q=${genre}`);

export default getMoviesCategory; 