import React from "react";
import { NavLink } from "react-router-dom";
import {routeMain as routeMoviesDetail} from "../../../../pages/MoviesDetail";
import { IMovies } from "types/IMovies";
import DateView from "components/DateView";
import './styles.scss';

interface IMoviesItemParams {
    item: IMovies;
} 

const MoviesItem: React.FC<IMoviesItemParams> = ({item}) => {
    return(
    <NavLink className="moviesItem" 
    to = {routeMoviesDetail(String(item.show.id))}>
            <img src={item.show.image ? item.show.image.medium : ""} alt={item.show.image ? item.show.image.medium : ""} className="image"></img>
            <div className="bottom">
                <h3 className="title">{item.show.name ? item.show.name: 'Untitled'}</h3>
                <div className="yearWrapper"><DateView
                    value={item.show.premiered ? item.show.premiered : 'Нет информации'}
                />
                &nbsp;
                <p>{item.show.network ? item.show.network.country.code : <span>Unknown</span>}</p>
                </div>
                <p className="genre">{item.show.genres.length > 0 ? item.show.genres.join(', ') : 'Нет информации'}</p>              
            </div>

    </NavLink>
      )
}

export default MoviesItem;