import React from "react";
import MoviesItemCategory from "./components/MoviesItem";
import { IMoviesDetail } from "types/IMoviesDetail";
import './styles.scss';

interface IMoviesListParams {
    list: IMoviesDetail[];
}

const MoviesListCategory: React.FC<IMoviesListParams> = ({list}) => (
    <div className="moviesList">
        {list.map((movies: IMoviesDetail) => (
            <MoviesItemCategory key={movies.id} item={movies}/>
        ))}
    </div>
)

export default MoviesListCategory;