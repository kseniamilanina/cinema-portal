import React from "react";
import { NavLink } from "react-router-dom";
import {routeMain as routeMoviesDetail} from "../../../../pages/MoviesDetail";
import { IMoviesDetail } from "types/IMoviesDetail";
import DateView from "components/DateView";
import './styles.scss';

interface IMoviesItemParams {
    item: IMoviesDetail;
} 

const MoviesItemCategory: React.FC<IMoviesItemParams> = ({item}) => {
    return(
    <NavLink className="moviesItem" 
    to = {routeMoviesDetail(String(item.id))}>
            <img src={item.image ? item.image.medium : ""} alt={item.image ? item.image.medium : ""} className="image"></img>
            <div className="bottom">
                <h3 className="title">{item.name ? item.name : 'Untitled'}</h3>
                <p className="genre">{item.genres ? item.genres.join(', ') : "Нет информации"}</p>              
            </div>

    </NavLink>
      )
}

export default MoviesItemCategory;