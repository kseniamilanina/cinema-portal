import React from 'react';
import MainPage, {routeMain as routeMainPage} from '../../pages/MainPage';
import Search, { routeMain as routeSearchPage} from '../../pages/Search';
import Categories, { routeMain as routeCategoriesPage} from '../../pages/Categories';
import About, { routeMain as routeAboutPage} from '../../pages/About';
import MoviesDetail, {routeMain as routeMoviesDetailPage} from '../../pages/MoviesDetail';
import { Route, Switch, Redirect } from 'react-router-dom';
import './styles.scss';
import Header from '../Header';
import Footer from 'components/Footer';

const AppContent = () => {
  return (
    <div className="mainWrapper">
        <Header/>
        <main>
            <Switch> 
                <Route exact path = {routeMainPage()} component={MainPage}/>
                <Route exact path = {routeCategoriesPage()} component={Categories}/>
                <Route exact path = {routeAboutPage()} component={About}/>
                <Route exact path = {routeSearchPage()} component={Search}/>
                <Route exact path = {routeMoviesDetailPage()} component={MoviesDetail}/>
                <Redirect
                    to = {{
                    pathname: routeMainPage()
                 }}
                 />
            </Switch>
        </main>
        <Footer/>
        
    </div>
  );
}

export default AppContent;
