import React from "react";
import './styles.scss';

const Footer = () => {
    return(
        <footer className="mainFooter">
            <div className="container">
            <div className="logo">

            </div>
            <div className="title">Приложение по поиску фильмов</div>
            <div className="made">Made by <br />
                <span>Ksenia Milanina</span>                
            </div>
            </div>

        </footer>
    )

}

export default Footer;