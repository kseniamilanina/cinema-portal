import React from 'react';
import { NavLink } from 'react-router-dom';
import { routeMain as routeMainPage} from '../../pages/MainPage';
import { routeMain as routeSearchPage} from '../../pages/Search';
import { routeMain as routeCategoriesPage} from '../../pages/Categories';
import { routeMain as routeAboutPage} from '../../pages/About';
import './styles.scss';



const Header = () => {
  return (
    <header className="mainHeader">
      <div className="container">
        <div className="logo"></div>
      <nav>
        <NavLink to={routeMainPage()} activeClassName={'linkActive'}>
          Главная
        </NavLink>
        <NavLink to={routeCategoriesPage()} activeClassName={'linkActive'}>
          Фильмы по категориям
        </NavLink>
        <NavLink to={routeAboutPage()} activeClassName={'linkActive'}>
          О нас
        </NavLink>
        <NavLink to={routeSearchPage()} activeClassName={'linkActive'}>
          Поиск
        </NavLink>
      </nav>
      </div>
    </header>
  );
}

export default Header;