import React from "react";

interface IButtonParams {
    onClick: () => void;
}
const Button:React.FC<IButtonParams> = ({onClick}) => <button onClick={onClick}></button>

export default Button;