import React from 'react';
import './styles.scss';
import AppContent from '../AppContent';

const App = () => {
  return (
    <div className="App">
        <AppContent/>
    </div>
  );
}

export default App;
