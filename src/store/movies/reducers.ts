import { AnyAction } from "redux";
import { IStore } from "../types";

const initialState = {
    list: [],
    categoryList: [],
    detail: undefined
}

const moviesReducer = (state: IStore = initialState, action: AnyAction) => {
    switch(action.type){
        case 'movies/setMovies' :
            return {...state, list: [...action.payload]}
        case 'movies/setMoviesCategory' :
            return {...state, categoryList: [...action.payload]}
        case 'movies/setMoviesDetail' :
            return {...state, detail: action.payload}
        default: 
            return state;
    }
}

export default moviesReducer;