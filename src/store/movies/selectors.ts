import { IStore } from "../types";

export const selectList = (state: {moviesReducer: IStore}): IStore['list'] => state.moviesReducer.list;

export const selectCategoryList = (state: {moviesReducer: IStore}): IStore['categoryList'] => state.moviesReducer.categoryList;

export const selectDetail = (state: {moviesReducer: IStore}): IStore['detail'] => state.moviesReducer.detail;