import {Dispatch} from 'redux';
import getMovies from '../../services/getMovies';
import getMoviesCategory from 'services/getMoviesCategory';
import getMoviesDetail from 'services/getMoviesDetail';
import { IStore } from '../types';

export const setMoviesAction = (list:IStore["list"]) => {
    return {
        type: 'movies/setMovies',
        payload: list,
    }
}

export const setMoviesCategoryAction = (categoryList:IStore["categoryList"]) => {
    return {
        type: 'movies/setMoviesCategory',
        payload: categoryList,
    }
}
export const setMoviesDetailAction = (detail:IStore["detail"]) => {
    return {
        type: 'movies/setMoviesDetail',
        payload: detail,
    }
}



export const loadMovies:any = (type:string) => async (dispatch: Dispatch) => {
    try {
        const response = await getMovies(type);
        dispatch(setMoviesAction(response.data))
    } catch(e) {
        console.log(e, 'произошла ошибка')
    }
}

export const loadMoviesCategory:any = (genre:string) => async (dispatch: Dispatch) => {
    try {
        const response = await getMoviesCategory(genre);
        dispatch(setMoviesCategoryAction(response.data))
    } catch(e) {
        console.log(e, 'произошла ошибка')
    }
}

export const loadMoviesDetail:any = (id:string) => async (dispatch: Dispatch) => {
    try {
        const response = await getMoviesDetail(id);
        dispatch(setMoviesDetailAction(response.data))
    } catch(e) {
        console.log(e, 'произошла ошибка')
    }
}