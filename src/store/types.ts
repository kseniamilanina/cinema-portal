import { IMovies } from "types/IMovies";
import { IMoviesDetail } from "types/IMoviesDetail";

export interface IStore {
    list: IMovies[]; 
    categoryList: IMoviesDetail[];
    detail: IMoviesDetail | undefined;
}