import React, {useEffect} from "react";
import routeMain from "./routes";
//redux
import {useDispatch, useSelector} from 'react-redux';
import { loadMoviesCategory } from "store/movies/actions";
import { selectCategoryList } from "store/movies/selectors";

import MoviesListCategory from "components/MoviesList copy";


import './styles.scss';

const Categories = () => { 
    const dispatch = useDispatch();
    const moviesList = useSelector(selectCategoryList);

    useEffect(() => {
        dispatch(loadMoviesCategory());
    }, [dispatch])
    return(
        <section className="categoryPage">
            <div className="whiteWrapper">
                <div className="categoryName">
                    Выбранная категория: <span>All films</span>
                </div>

                {moviesList.length > 0 && <MoviesListCategory list = {moviesList.slice(0,16)}/>}
            </div>
        </section>
    )
    
}

export {routeMain};
export default Categories;