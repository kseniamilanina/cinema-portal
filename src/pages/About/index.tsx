import React from "react";
import routeMain from "./routes";
import './styles.scss';

const About = () => {
    return (
        <div className="aboutWrapper">
            <div className="image"></div>
            <div className="description">
                <h2 className="title">MOVIESinfo</h2>
                <p className="text"> Our service MOVIESinfo represents the full information about a wide range of films. 
                You can serach films on different categories and genres. Detailed information about films includes the country of production, 
                the premiered year, gentres, raiting and common description. Our service will be usefull for you, if you try to find interesting appropriate film 
                to see every evening. We have films for every taste! Follow us and surf films. 
                </p>
            </div>
        </div>
    )
}

export {routeMain};
export default About;
