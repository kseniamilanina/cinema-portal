import React, {useEffect} from "react";
import routeMain from "./routes";
//redux
import {useDispatch, useSelector} from 'react-redux';
import { loadMovies } from "../../store/movies/actions";
import { selectList } from "../../store/movies/selectors";


import MoviesList from "../../components/MoviesList";


import './styles.scss';

const MainPage = () => { 
    const dispatch = useDispatch();
    const moviesList = useSelector(selectList);

    useEffect(() => {
        dispatch(loadMovies("girls"));
    }, [dispatch])
    
    return(
        <section className="mainPage">
            <div className="offer">
                <h1 className="title">MOVIESinfo</h1>
                <p className="text">Самый популярный портал о фильмах </p>
            </div>
            <div className="whiteWrapper">
                {moviesList.length > 0 && <MoviesList list = {moviesList.slice(0,8)}/>}
            </div>
        </section>
    )
    
}

export {routeMain};
export default MainPage;