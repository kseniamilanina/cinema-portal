import React, {useState, useEffect} from "react";
import { useParams } from "react-router-dom";
import routeMain from "./routes";
import getMoviesDetail from "services/getMoviesDetail";
import { ID } from "../../types/ID";
import { IMoviesDetail } from "types/IMoviesDetail";
import DateView from "components/DateView";

import {useDispatch, useSelector} from 'react-redux';
import { loadMoviesDetail } from "store/movies/actions";
import { selectDetail } from "store/movies/selectors";

import './styles.scss';

const MoviesDetail = () => {
    const {id} = useParams<ID>();
    //const [movies, setMovies] = useState<IMoviesDetail | undefined> (undefined);

    const dispatch = useDispatch();
    const movies = useSelector(selectDetail);

    useEffect(() => {
        dispatch(loadMoviesDetail(id));
    }, [dispatch])

    // useEffect(() => {
    //     getMoviesDetail(id).then(response=>{
    //         const currentMovie = response.data;
    //     setMovies(currentMovie);
    //     })
        
    // }, [id])

    return(
        <section className="moviesDetailPage">
            {movies ? (
                <div className="moviesDetailWrapper">
                    <div className="image">
                        <img src={movies.image.medium} alt={movies.image.medium} />
                    </div>
                    <div className="description">
                        <div className="descTop">
                            <h2 className="title">{movies.name}</h2>
                            <div className="rating">{movies.rating.average}/10</div>
                        </div>
                        <div className="descBottom">
                            <div className="subTitleWrapper">
                                <div className="subTitle">ГОД ВЫХОДА:</div>
                                <div className="subTitle">СТРАНА:</div>
                                <div className="subTitle">ЖАНР:</div>
                                <div className="subTitle">ОПИСАНИЕ:</div>                               
                            </div>
                            <div className="valueWrapper">
                                <div className="value"><DateView
                                        value = {movies.premiered}
                                />
                                </div>
                                <div className="value">{movies.network ? movies.network.country.name : 'Unknown'}</div>
                                <div className="value">{movies.genres ? movies.genres.join(', '): 'Нет информации'}</div>
                                <span dangerouslySetInnerHTML={{__html: movies.summary ? movies.summary: 'Нет информации'}}></span>
                            </div>

                        </div>
                        
                    </div>
                </div>
            ) : <></>}

        </section>
    )
}

export {routeMain};
export default MoviesDetail;