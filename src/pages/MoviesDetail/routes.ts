const routeMain = (id = ":id") => `/moviesDetail/${id}`;

export default routeMain;