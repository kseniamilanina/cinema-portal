import React, {useState, useEffect} from "react";
import routeMain from "./routes";
import {useDispatch, useSelector} from 'react-redux'; 
import { loadMovies } from "../../store/movies/actions";
import { selectList } from "../../store/movies/selectors";

import MoviesList from "../../components/MoviesList";
import Input from "components/Input";
import Button from "components/Button";
import './styles.scss';

const Search = () => {
    const dispatch = useDispatch();
    const moviesList = useSelector(selectList);
    const [genre, setGenre] = useState("");
    const [value, setValue] = useState("");

    useEffect(() => {
        dispatch(loadMovies(genre));
    }, [genre, dispatch])

    const getValue = (e:React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);
    }
    const handleChange = () => {
        setGenre(value);
    }
    
    return(
        <section className="categoryPage searchPage">
            <div className="whiteWrapper">
                <div className="searchTitle">
                    Поиск по категории 
                </div>
                <div className="inputWrapper">
                    <Input
                    onChange={getValue}
                    placeholder="Example: drama"
                    />
                    <Button
                    onClick = {handleChange}
                    />
                </div>
                <div className="results">
                    Результаты поиска:
                </div>
                {genre.length == 0 ? <div className="zeroSearch">
                    <p>Введите поисковой запрос 
                    для отображения результатов</p>
                </div> : moviesList.length > 0 ? <MoviesList list = {moviesList}/>: 
                <div className="zeroSearch"><p>Подождите, идет поиск</p></div>
                 }
                
            </div>
        </section>
    )
}

export {routeMain};
export default Search;